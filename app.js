var express = require('express');
var path = require('path');
var gpio = require('./wrappers/pi-gpio-wrapper');
var app = express();

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.render('index.html');
});
app.get('/api/pins/:pinNumber', function(req, res) {
	var pinNumber = req.params.pinNumber;
	gpio.open(pinNumber, "input", function(err) {
		gpio.read(pinNumber, function(err, value) {
			var result = value;
			gpio.close(pinNumber, function() {
				res.json({value: result});
			});
		});
	});
});
var testLoop = null;
app.get('/api/test', function(req, res) {
	if(testLoop != null) {
		clearInterval(testLoop);
		testLoop = null;
	} else {
		testLoop = setInterval(function() {
			gpio.toggle(8, function(value) {
				// done
			});
		}, 1000);
	}
	res.send('Testing Pins');
});
app.post('/api/pins/:pinNumber', function(req, res) {
	var pinNumber = req.params.pinNumber;
	gpio.toggle(pinNumber, function(value) {
		res.json({value: value});
	});
});
app.post('/api/pins/:pinNumber/test', function(req, res) {
	var pinNumber = req.params.pinNumber;
	gpio.toggle(pinNumber, function(value) {
		gpio.toggle(pinNumber, function(value) {
			res.send(pinNumber + ' tested');
		});
	});
});


app.listen(app.get('port'), function() {
	console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});
