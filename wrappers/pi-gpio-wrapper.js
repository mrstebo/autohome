var gpio = {};

if(require('fs').existsSync('/proc/cpuinfo')) {
	gpio = require('pi-gpio');
} else {
	var pinState = function(pinNumber, direction, value) {
		this.pinNumber = pinNumber;
		this.direction = direction;
		this.value = value;
	};
	gpio = {
		pinStates: [],
		open: function(pinNumber, options, callback) {
			callback(null);
		},
		close: function(pinNumber, callback) {
			callback();
		},
		setDirection: function(pinNumber, direction, callback) {
			this.pinStates[pinNumber-1].direction = value;
			callback(null);
		},
		getDirection: function(pinNumber, callback) {
			callback(null, this.pinStates[pinNumber-1].direction);
		},
		read: function(pinNumber, callback) {
			callback(null, this.pinStates[pinNumber-1].value);
		},
		write: function(pinNumber, value, callback) {
			this.pinStates[pinNumber-1].value = value;
			callback(null);
		}
	};

	for (i = 1; i <= 40; i++) {
		gpio.pinStates.push(new pinState(i, "output", 0));
	}

	console.log('Using mock pi-gpio as we are not on the pi');
}

gpio.toggle = function(pinNumber, callback) {
	var obj = this;
	obj.open(pinNumber, "input", function(err) {
		obj.read(pinNumber, function(err, value) {
			var newValue = (value == 0) ? 1 : 0;
			obj.close(pinNumber, function() {
				obj.open(pinNumber, "output", function(err) {
					obj.write(pinNumber, newValue, function() {
						obj.close(pinNumber, function() {
							callback(newValue);
						});
					});
				});
			});
		});
	});
};

module.exports = gpio;
